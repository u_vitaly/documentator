-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 15 2022 г., 09:11
-- Версия сервера: 5.6.41-log
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `documentator`
--

-- --------------------------------------------------------

--
-- Структура таблицы `document`
--

CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(20) DEFAULT NULL,
  `updated_at` int(20) DEFAULT NULL,
  `hasNeiroAdaptation` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `document`
--

INSERT INTO `document` (`id`, `name`, `created_at`, `updated_at`, `hasNeiroAdaptation`) VALUES
(365, '001.rtf', 1652645241, 1652645241, NULL),
(366, '002.rtf', 1652645241, 1652645241, NULL),
(367, '003.rtf', 1652645241, 1652645241, NULL),
(368, '004.rtf', 1652645241, 1652645241, NULL),
(369, '005.rtf', 1652645241, 1652645241, NULL),
(370, '006.rtf', 1652645241, 1652645241, NULL),
(371, '007.rtf', 1652645241, 1652645241, NULL),
(372, '008.rtf', 1652645241, 1652645241, NULL),
(373, '009.rtf', 1652645241, 1652645241, NULL),
(374, '010.rtf', 1652645241, 1652645241, NULL),
(375, '011.rtf', 1652645241, 1652645241, NULL),
(376, '012.rtf', 1652645241, 1652645241, NULL),
(377, '014.rtf', 1652645241, 1652645241, NULL),
(378, '019.rtf', 1652645241, 1652645241, NULL),
(379, '023.rtf', 1652645241, 1652645241, NULL),
(380, '025.rtf', 1652645241, 1652645241, NULL),
(381, '026.rtf', 1652645241, 1652645241, NULL),
(382, '027.rtf', 1652645241, 1652645241, NULL),
(383, '028.rtf', 1652645241, 1652645241, NULL),
(384, '029.rtf', 1652645241, 1652645241, NULL),
(385, '030.rtf', 1652645241, 1652645241, NULL),
(386, '031.rtf', 1652645241, 1652645241, NULL),
(387, '034.rtf', 1652645241, 1652645241, NULL),
(388, '035.rtf', 1652645241, 1652645241, NULL),
(389, '039.rtf', 1652645241, 1652645241, NULL),
(390, '058.rtf', 1652645241, 1652645241, NULL),
(391, '060.rtf', 1652645241, 1652645241, NULL),
(392, '061.rtf', 1652645241, 1652645241, NULL),
(393, '063.rtf', 1652645241, 1652645241, NULL),
(394, '068.rtf', 1652645241, 1652645241, NULL),
(395, '079.rtf', 1652645241, 1652645241, NULL),
(396, '084.rtf', 1652645241, 1652645241, NULL),
(397, '085.rtf', 1652645241, 1652645241, NULL),
(398, '104.rtf', 1652645241, 1652645241, NULL),
(399, '152.rtf', 1652645241, 1652645241, NULL),
(400, '180.rtf', 1652645241, 1652645241, NULL),
(401, '219.rtf', 1652645241, 1652645241, NULL),
(402, '342.rtf', 1652645241, 1652645241, NULL),
(403, '343.rtf', 1652645241, 1652645241, NULL),
(404, '424.rtf', 1652645241, 1652645241, NULL),
(405, '463.rtf', 1652645241, 1652645241, NULL),
(406, '493.rtf', 1652645241, 1652645241, NULL),
(407, '496.rtf', 1652645241, 1652645241, NULL),
(408, '500.rtf', 1652645241, 1652645241, NULL),
(409, '509.rtf', 1652645241, 1652645241, NULL),
(410, '515.rtf', 1652645241, 1652645241, NULL),
(411, '521.rtf', 1652645241, 1652645241, NULL),
(412, '527.rtf', 1652645241, 1652645241, NULL),
(413, '540.rtf', 1652645241, 1652645241, NULL),
(414, '554.rtf', 1652645241, 1652645241, NULL),
(415, '562.rtf', 1652645241, 1652645241, NULL),
(416, '620.rtf', 1652645241, 1652645241, NULL),
(417, '646.rtf', 1652645241, 1652645241, NULL),
(418, '657.rtf', 1652645241, 1652645241, NULL),
(419, '662.rtf', 1652645241, 1652645241, NULL),
(420, '702.rtf', 1652645241, 1652645241, NULL),
(421, '705.rtf', 1652645241, 1652645241, NULL),
(423, '.', 1652645902, 1652645902, NULL),
(424, '..', 1652645902, 1652645902, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `idUserSender` int(12) DEFAULT NULL,
  `idUserDest` int(12) DEFAULT NULL,
  `text` varchar(511) DEFAULT NULL,
  `isChecked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1652564415),
('m220514_215201_create_user_table', 1652565175),
('m220514_215426_create_document_table', 1652565507),
('m220514_215909_create_user2document_table', 1652565588),
('m220514_221554_create_test_table', 1652566683),
('m220514_221826_create_test2user_table', 1652566715),
('m220514_222342_create_message_table', 1652567028),
('m220514_223211_add_isAdmin_column_to_user_table', 1652567554),
('m220515_095354_add_authKey_column_to_user_table', 1652608443),
('m220515_173816_add_created_at_column_to_user_table', 1652636303),
('m220515_174003_add_updated_at_column_to_user_table', 1652636408),
('m220515_194514_add_created_at_column_to_document_table', 1652643938),
('m220515_194533_add_updated_at_column_to_document_table', 1652643938),
('m220515_235854_add_updated_at_column_to_user2document_table', 1652659160),
('m220515_235915_add_created_at_column_to_user2document_table', 1652659160),
('m220614_001843_create_requester_table', 1655165949),
('m220614_002950_add_apiKey_column_to_requester_table', 1655166601),
('m220614_015234_add_spendedTime_column_to_user2document_table', 1655171987),
('m220614_015323_add_isLearned_column_to_user2document_table', 1655171987),
('m220614_015833_add_hasNeiroAdaptation_column_to_document_table', 1655171987),
('m220614_020027_drop_hasNeiroAdaptation_column_from_user2document_table', 1655172035);

-- --------------------------------------------------------

--
-- Структура таблицы `requester`
--

CREATE TABLE `requester` (
  `id` int(11) NOT NULL,
  `email` varchar(127) DEFAULT NULL,
  `isAccepted` tinyint(1) DEFAULT NULL,
  `apiKey` varchar(127) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(63) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `test2user`
--

CREATE TABLE `test2user` (
  `id` int(11) NOT NULL,
  `idTest` int(12) DEFAULT NULL,
  `idUser` int(12) DEFAULT NULL,
  `isSuccess` tinyint(1) DEFAULT NULL,
  `percentOfSuccess` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(63) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `password` varchar(63) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `authKey` varchar(32) NOT NULL,
  `created_at` int(20) DEFAULT NULL,
  `updated_at` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `isAdmin`, `authKey`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'vital.161@mail.ru', 'vg16jerio', 1, '', NULL, NULL),
(2, NULL, NULL, NULL, NULL, '', 1652636413, 1652636413),
(3, NULL, NULL, NULL, NULL, '', 1652636424, 1652636424),
(5, 'Vital161', 'vitaly.161@mail.ru', 'vg16jerio', 1, '', NULL, NULL),
(6, NULL, NULL, NULL, NULL, '', 1652658752, 1652658752),
(7, NULL, NULL, NULL, NULL, '', 1652658798, 1652658798),
(8, NULL, NULL, NULL, NULL, '', 1652658908, 1652658908),
(9, NULL, NULL, NULL, NULL, '', 1652658944, 1652658944),
(10, NULL, NULL, NULL, NULL, '', 1652658999, 1652658999);

-- --------------------------------------------------------

--
-- Структура таблицы `user2document`
--

CREATE TABLE `user2document` (
  `id` int(11) NOT NULL,
  `idUser` int(12) DEFAULT NULL,
  `idDocument` int(12) DEFAULT NULL,
  `updated_at` int(20) DEFAULT NULL,
  `created_at` int(20) DEFAULT NULL,
  `spendedTime` varchar(63) DEFAULT NULL,
  `isLearned` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user2document`
--

INSERT INTO `user2document` (`id`, `idUser`, `idDocument`, `updated_at`, `created_at`, `spendedTime`, `isLearned`) VALUES
(1, NULL, NULL, 1652659164, 1652659164, NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-message-idUserSender` (`idUserSender`),
  ADD KEY `idx-message-idUserDest` (`idUserDest`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `requester`
--
ALTER TABLE `requester`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `test2user`
--
ALTER TABLE `test2user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-test2user-idTest` (`idTest`),
  ADD KEY `idx-test2user-idUser` (`idUser`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user2document`
--
ALTER TABLE `user2document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user2document-idUser` (`idUser`),
  ADD KEY `idx-user2document-idDocument` (`idDocument`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=425;

--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requester`
--
ALTER TABLE `requester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `test2user`
--
ALTER TABLE `test2user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `user2document`
--
ALTER TABLE `user2document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk-message-idUserDest` FOREIGN KEY (`idUserDest`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-message-idUserSender` FOREIGN KEY (`idUserSender`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `test2user`
--
ALTER TABLE `test2user`
  ADD CONSTRAINT `fk-test2user-idTest` FOREIGN KEY (`idTest`) REFERENCES `test` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-test2user-idUser` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user2document`
--
ALTER TABLE `user2document`
  ADD CONSTRAINT `fk-user2document-idDocument` FOREIGN KEY (`idDocument`) REFERENCES `document` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-user2document-idUser` FOREIGN KEY (`idUser`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

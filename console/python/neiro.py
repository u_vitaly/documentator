#!/usr/bin/env python
from razdel import sentenize
from razdel import tokenize

from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV



import re
import os
import sys
import pandas as pd

from striprtf.striprtf import rtf_to_text
# from striprtf.striprtf import read_rtf

import PyPDF2


data = pd.read_csv(os.getcwd()+"\web\documents\dataset\data.csv", usecols=['sentience', 'title', 'subtitle'])
data.head()
p = pd.DataFrame(data['title'].value_counts())

text = sys.argv[1]
file = open(os.getcwd() + "\web\documents\\txtFiles\\" + text, 'r',encoding='utf-8').read().replace('\n', '')
sents = list(sentenize(file))

X_train, X_valid, y_train, y_valid = train_test_split(data['sentience'], data['title'], test_size=0.2, random_state=40)
X_train.shape
X_valid.shape
X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.2, random_state=40)
X_test.shape

sgd_ppl_clf = Pipeline([
    ('tfidf', TfidfVectorizer()),
    ('sgd_clf', SGDClassifier(random_state=42))
])

knb_ppl_clf = Pipeline([
    ('tfidf', TfidfVectorizer()),
    ('knb_clf', KNeighborsClassifier(n_neighbors=4))
])

sgd_ppl_clf.fit(X_train, y_train)

predicted_sgd = sgd_ppl_clf.predict(X_test)

parameters = { 
              'sgd_clf__loss':['hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron'],
              'sgd_clf__class_weight':[None, 'balanced'],
              'sgd_clf__penalty':[None, 'l2', 'l1', 'elasticnet'],
              'tfidf__strip_accents':['ascii', 'unicode', None],
               'tfidf__ngram_range':[(1,2), (1,3), (1,4)]
              }
model = GridSearchCV(sgd_ppl_clf, parameters, cv=4, n_jobs=-1).fit(X_train, y_train)

sgd_ppl_clf = Pipeline([
    ('tfidf', TfidfVectorizer(ngram_range=(1, 4), strip_accents='unicode')),
    ('sgd_clf', SGDClassifier(penalty='elasticnet', class_weight='balanced', random_state=42))
])

sgd_ppl_clf.fit(X_train, y_train)

predicted_sgd = sgd_ppl_clf.predict(X_test)

knb_ppl_clf.fit(X_train, y_train)

predicted_sgd = knb_ppl_clf.predict(X_test)

predicted_sgd_val = sgd_ppl_clf.predict(X_valid)

result = dict()
temp = ''
for sent in sents:
        predicted_sgd_val = sgd_ppl_clf.predict({sent.text})
        # if(predicted_sgd_val) == 'заголовок':
        #     temp = predicted_sgd_val
        #     switch(temp){
        #         case 
        #     }
        if re.fullmatch(r'\d\D\d\D', sent.text) or re.fullmatch(r'\d\D', sent.text)  or re.fullmatch(r'\d\D\d', sent.text):
            continue
        else:
            result[sent.text] = predicted_sgd_val
print(result)

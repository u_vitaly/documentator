#!/usr/bin/env python
from razdel import sentenize
from razdel import tokenize

from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.model_selection import GridSearchCV

import re
import pandas as pd

from striprtf.striprtf import rtf_to_text
# from striprtf.striprtf import read_rtf

import PyPDF2

from sys import argv

a = "Its works!"
print(a)
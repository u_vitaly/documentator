<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user2document}}`.
 */
class m220515_235915_add_created_at_column_to_user2document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user2document}}', 'created_at', $this->integer(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user2document}}', 'created_at');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%test2user}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%document}}`
 */
class m220626_184728_add_idDocument_column_to_test2user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%test2user}}', 'idDocument', $this->integer(11));

        // creates index for column `idDocument`
        $this->createIndex(
            '{{%idx-test2user-idDocument}}',
            '{{%test2user}}',
            'idDocument'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-test2user-idDocument}}',
            '{{%test2user}}',
            'idDocument',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-test2user-idDocument}}',
            '{{%test2user}}'
        );

        // drops index for column `idDocument`
        $this->dropIndex(
            '{{%idx-test2user-idDocument}}',
            '{{%test2user}}'
        );

        $this->dropColumn('{{%test2user}}', 'idDocument');
    }
}

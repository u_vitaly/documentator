<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%message}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%user}}`
 */
class m220514_222342_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'idUserSender' => $this->integer(12),
            'idUserDest' => $this->integer(12),
            'text' => $this->string(511),
            'isChecked' => $this->boolean(),
        ]);

        // creates index for column `idUserSender`
        $this->createIndex(
            '{{%idx-message-idUserSender}}',
            '{{%message}}',
            'idUserSender'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-message-idUserSender}}',
            '{{%message}}',
            'idUserSender',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `idUserDest`
        $this->createIndex(
            '{{%idx-message-idUserDest}}',
            '{{%message}}',
            'idUserDest'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-message-idUserDest}}',
            '{{%message}}',
            'idUserDest',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-message-idUserSender}}',
            '{{%message}}'
        );

        // drops index for column `idUserSender`
        $this->dropIndex(
            '{{%idx-message-idUserSender}}',
            '{{%message}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-message-idUserDest}}',
            '{{%message}}'
        );

        // drops index for column `idUserDest`
        $this->dropIndex(
            '{{%idx-message-idUserDest}}',
            '{{%message}}'
        );

        $this->dropTable('{{%message}}');
    }
}

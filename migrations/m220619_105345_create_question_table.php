<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test}}`
 */
class m220619_105345_create_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'idTest' => $this->integer(11),
            'name' => $this->string(255),
        ]);

        // creates index for column `idTest`
        $this->createIndex(
            '{{%idx-question-idTest}}',
            '{{%question}}',
            'idTest'
        );

        // add foreign key for table `{{%test}}`
        $this->addForeignKey(
            '{{%fk-question-idTest}}',
            '{{%question}}',
            'idTest',
            '{{%test}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test}}`
        $this->dropForeignKey(
            '{{%fk-question-idTest}}',
            '{{%question}}'
        );

        // drops index for column `idTest`
        $this->dropIndex(
            '{{%idx-question-idTest}}',
            '{{%question}}'
        );

        $this->dropTable('{{%question}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%user2document}}`.
 */
class m220614_020027_drop_hasNeiroAdaptation_column_from_user2document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user2document}}', 'hasNeiroAdaptation');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user2document}}', 'hasNeiroAdaptation', $this->boolean());
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%requester}}`.
 */
class m220614_001843_create_requester_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%requester}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(127),
            'isAccepted' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%requester}}');
    }
}

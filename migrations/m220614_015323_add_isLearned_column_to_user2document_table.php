<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user2document}}`.
 */
class m220614_015323_add_isLearned_column_to_user2document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user2document}}', 'isLearned', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user2document}}', 'isLearned');
    }
}

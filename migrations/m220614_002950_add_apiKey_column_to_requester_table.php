<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%requester}}`.
 */
class m220614_002950_add_apiKey_column_to_requester_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%requester}}', 'apiKey', $this->string(127));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%requester}}', 'apiKey');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user2document}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%document}}`
 */
class m220514_215909_create_user2document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user2document}}', [
            'id' => $this->primaryKey(),
            'idUser' => $this->integer(12),
            'idDocument' => $this->integer(12),
            'hasNeiroAdaptation' => $this->boolean(),
        ]);

        // creates index for column `idUser`
        $this->createIndex(
            '{{%idx-user2document-idUser}}',
            '{{%user2document}}',
            'idUser'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user2document-idUser}}',
            '{{%user2document}}',
            'idUser',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `idDocument`
        $this->createIndex(
            '{{%idx-user2document-idDocument}}',
            '{{%user2document}}',
            'idDocument'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-user2document-idDocument}}',
            '{{%user2document}}',
            'idDocument',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user2document-idUser}}',
            '{{%user2document}}'
        );

        // drops index for column `idUser`
        $this->dropIndex(
            '{{%idx-user2document-idUser}}',
            '{{%user2document}}'
        );

        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-user2document-idDocument}}',
            '{{%user2document}}'
        );

        // drops index for column `idDocument`
        $this->dropIndex(
            '{{%idx-user2document-idDocument}}',
            '{{%user2document}}'
        );

        $this->dropTable('{{%user2document}}');
    }
}

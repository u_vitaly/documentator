<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user2document}}`.
 */
class m220614_015234_add_spendedTime_column_to_user2document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user2document}}', 'spendedTime', $this->string(63));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user2document}}', 'spendedTime');
    }
}

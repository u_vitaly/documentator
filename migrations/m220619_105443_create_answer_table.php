<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%answer}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%question}}`
 */
class m220619_105443_create_answer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%answer}}', [
            'id' => $this->primaryKey(),
            'idQuestion' => $this->integer(11),
            'name' => $this->string(255),
            'isTrue' => $this->boolean(),
        ]);

        // creates index for column `idQuestion`
        $this->createIndex(
            '{{%idx-answer-idQuestion}}',
            '{{%answer}}',
            'idQuestion'
        );

        // add foreign key for table `{{%question}}`
        $this->addForeignKey(
            '{{%fk-answer-idQuestion}}',
            '{{%answer}}',
            'idQuestion',
            '{{%question}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%question}}`
        $this->dropForeignKey(
            '{{%fk-answer-idQuestion}}',
            '{{%answer}}'
        );

        // drops index for column `idQuestion`
        $this->dropIndex(
            '{{%idx-answer-idQuestion}}',
            '{{%answer}}'
        );

        $this->dropTable('{{%answer}}');
    }
}

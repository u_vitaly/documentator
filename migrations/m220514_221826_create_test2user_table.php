<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test2user}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test}}`
 * - `{{%user}}`
 */
class m220514_221826_create_test2user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test2user}}', [
            'id' => $this->primaryKey(),
            'idTest' => $this->integer(12),
            'idUser' => $this->integer(12),
            'isSuccess' => $this->boolean(),
            'percentOfSuccess' => $this->integer(3),
        ]);

        // creates index for column `idTest`
        $this->createIndex(
            '{{%idx-test2user-idTest}}',
            '{{%test2user}}',
            'idTest'
        );

        // add foreign key for table `{{%test}}`
        $this->addForeignKey(
            '{{%fk-test2user-idTest}}',
            '{{%test2user}}',
            'idTest',
            '{{%test}}',
            'id',
            'CASCADE'
        );

        // creates index for column `idUser`
        $this->createIndex(
            '{{%idx-test2user-idUser}}',
            '{{%test2user}}',
            'idUser'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-test2user-idUser}}',
            '{{%test2user}}',
            'idUser',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test}}`
        $this->dropForeignKey(
            '{{%fk-test2user-idTest}}',
            '{{%test2user}}'
        );

        // drops index for column `idTest`
        $this->dropIndex(
            '{{%idx-test2user-idTest}}',
            '{{%test2user}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-test2user-idUser}}',
            '{{%test2user}}'
        );

        // drops index for column `idUser`
        $this->dropIndex(
            '{{%idx-test2user-idUser}}',
            '{{%test2user}}'
        );

        $this->dropTable('{{%test2user}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%document}}`
 */
class m220618_183606_add_current_document_id_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'current_document_id', $this->integer(11));

        // creates index for column `current_document_id`
        $this->createIndex(
            '{{%idx-user-current_document_id}}',
            '{{%user}}',
            'current_document_id'
        );

        // add foreign key for table `{{%document}}`
        $this->addForeignKey(
            '{{%fk-user-current_document_id}}',
            '{{%user}}',
            'current_document_id',
            '{{%document}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%document}}`
        $this->dropForeignKey(
            '{{%fk-user-current_document_id}}',
            '{{%user}}'
        );

        // drops index for column `current_document_id`
        $this->dropIndex(
            '{{%idx-user-current_document_id}}',
            '{{%user}}'
        );

        $this->dropColumn('{{%user}}', 'current_document_id');
    }
}

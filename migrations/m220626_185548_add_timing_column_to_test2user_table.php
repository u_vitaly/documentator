<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%test2user}}`.
 */
class m220626_185548_add_timing_column_to_test2user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%test2user}}', 'timing', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%test2user}}', 'timing');
    }
}

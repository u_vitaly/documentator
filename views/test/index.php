<?php 

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\TestDropdown;

$params = ['prompt'=>'Выберите документ'];
echo TestDropdown::widget(['documents'=>$documents->find()->byUserId($userId)->all(),'caret'=>true]);
echo Html::button(Html::a('Тестирование',Url::to('test/')),['class'=>'btn btn_test btn_test__wrong']);
?>
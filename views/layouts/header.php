<?php use yii\helpers\Html;
?>
<header>
            <div class="header__logo p-20">
                <img src='/images/logo.png'>
                <a href="/site/index">Documentator</a>
            </div>
            <div class="header__auth p-20">
                <div class="header__login">
                    <?php if(Yii::$app->user->isGuest)
                    {
                        echo Html::a('Вход', ['/site/login']);
                    } else {
                        echo Html::a($user->username, ['/user/index']);
                        echo Html::a('Выйти', ['/site/logout'],['data-method' => 'post']);
                    }
                    ?>
                </div>
            </div>
    </header>
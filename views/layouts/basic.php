<?php
use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<?php $this->beginBody() ?>
<body>
    <div class="wrap">
        <div class="container">
            <ul class="nav nav-pills">
                <li class="nav-item">
                  <?= Html::a('Главная','/web/') ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Статья',['post/test']) ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Статья',['post/show']) ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Главная','/web/') ?>
                </li>
            </ul>
        </div>
    </div>

<?php if(isset($this->blocks['block1'])): ?>
    <?php debug($this->blocks) ?>
<?php endif;?>
    <?= $content ?>
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
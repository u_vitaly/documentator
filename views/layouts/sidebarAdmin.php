
<?php use app\widgets\CustomSideBar;

echo CustomSideBar::widget(
    [
        'titlesActions'=>
        [
            'Пользователи'=>'/admin',
            'Документы'=>'/admin/document',
            'Тестирование'=>'/admin/test'
        ],  
        'titlesIcons'=>
        [
            'Пользователи'=>'/icons/user.png',
            'Документы'=>'/icons/290137_clipboard_document_file_list_report_icon.svg',
            'Тестирование'=>'/icons/approval.png'
        ]
    ]) 
?>
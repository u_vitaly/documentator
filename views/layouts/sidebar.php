<?php use app\widgets\CustomSideBar;

echo CustomSideBar::widget(['titlesActions'=>['Главная'=>'/site/index','Мои документы'=>'/documents/index','Тестирование'=>'/test/index','Прогресс'=>'/progress/index','Уведомления'=>'/notification/index'],
                                              'titlesIcons'=>['Главная'=>'/icons/216242_home_icon.svg','Мои документы'=>'/icons/290137_clipboard_document_file_list_report_icon.svg','Тестирование'=>'/icons/approval.png','Прогресс'=>'/icons/rise.png','Уведомления'=>'/icons/notification.png']
                                              ]) ?>
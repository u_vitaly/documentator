<?php

use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url; 
use yii\helpers\Html;
use app\widgets\TimeFormatter; 

/* @var $this yii\web\View */

$this->title = 'Обработчик докуметации';
?>
<div class="site-index">
    <div class="info_box current_document">
        <p>Текущий документ:</p>
        <?= Html::a($document->name,Url::to(['document', 'id' => $document->id])) ?>
        <?= Html::a('Продолжить чтение',Url::to(['document', 'id' => $document->id])) ?>
    </div>
    <div class="info_box spended_time">
        <?= TimeFormatter::widget(['seconds'=>$spendedTime]) ?>
    </div>
    <div class="info_box start_document">
        <p>Исходный документ:</p>
        <?= Html::a('Скачать',Url::to(['document', 'id' => $document->id])) ?>
    </div>
</div>

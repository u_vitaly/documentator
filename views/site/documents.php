<?php

use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url; 

/* @var $this yii\web\View */

$this->title = 'Документы';
?>

<div class="site-index">

<?php echo $this->render('_table',['tableTitle'=>'Необработанные документы','userId'=> $userId,'where'=>['hasNeiroAdaptation'=>0],'columns'=>['Название'=>'db_name','Скачать'=>'url_to_'],'documents'=>$documents]) ?>

<?php echo $this->render('_table',['tableTitle'=>'Готовые документы','userId'=> $userId,'where'=>['hasNeiroAdaptation'=>1],'columns'=>['Название'=>'db_name','Приступить к изучению'=>'url_to_'],'documents'=>$documents]) ?>

<?php echo $this->render('_table',['tableTitle'=>'Документы в процессе изучения','userId'=> $userId,'where'=>['isLearned'=>0],'columns'=>['Название'=>'db_name','Продолжить изучение'=>'url_to_'],'documents'=>$documents]) ?> <!-- TODO: Need checking for spended time (must not null) -->

<?php echo $this->render('_table',['tableTitle'=>'Изученные документы','userId'=> $userId,'where'=>['isLearned'=>1],'columns'=>['Название'=>'db_name','Затраченное время'=>'db_spended time'],'documents'=>$documents]) ?>

</div>

<?php

use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use app\models\Document;

/* @var $documents \app\queries\DocumentQuery */
?>

<?php
    $form = ActiveForm::begin();
?>
    
    <span style="font-size: 20px; font-weight: 900; color: #000;"><?=$tableTitle?></span>
    <table class="table table-bordered">
        <thead>
            <trow>
                <td>*</td>
                <?php foreach($columns as $key=>$value): ?>
                    <?= '<td>'.$key.'</td>' ?>
                <?php endforeach; ?>
            </trow>
        </thead>

        <?php
            $dataProvider = new ActiveDataProvider([
            'query' =>  $documents->find()->byUserId($userId)->where($where),
            'pagination' => [
            'pageSize' => 5,
            ],
            ]);
            
            echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'tables\_documents',
            'viewParams' => ['columns' => $columns,'tableTitle' => $tableTitle]
            ]);   
        ?>
    </table>
<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveField;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $documents \app\models\Document */
/* @var $this yii\web\View */

$property = '';
?>
<tr id=<?= $tableTitle.'_'.$model->id?> >
    <td><?= $model->id ?></td>
    
    <?php foreach($columns as $column): ?>
    <?php 
    if(str_starts_with($column,'hyper_')) {
        if($model->hasProperty(substr($column,6))) {          
            echo '<td>'.$model->getAttribute([substr($column,6)].'</td>');
        } else {
            echo '<td>'.'Not found'.'</td>';
        }
    } else {
        echo '<td>'.$model->getAttribute($column.'</td>');
    }
    ?>
    <?php endforeach; ?>
</tr>


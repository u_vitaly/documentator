<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveField;
use yii\widgets\ActiveForm;
use yii\web\View;

/* @var $model \common\models\User */
?>

<tr id="user_<?= $model->id?>">
    <td><?=$model->id ?></td>
    <td><?=$model->username ?></td>
    <td><?=$model->email ?></td>
    <td><?=$model->password ?></td>
    <td><button type="button" class="add_user_button btn btn-primary greenLight mb-10 " data-bs-toggle="modal" data-id=<?=$model->id?> data-bs-target="#updateModal" data-bs-whatever="@getbootstrap">Изменить пользователя</button></td>
</tr>

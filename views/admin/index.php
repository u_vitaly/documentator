<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use app\models\User;

/* @var $this yii\web\View */

$this->title = 'Административная панель';
?>
<div class="modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Создание пользователя</h2> 
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            </div>
            <div class="modal-body">
                <?php $formNewUser = ActiveForm::begin(); ?>
                <?= $formNewUser->field($model,'email') ?>
                <?= $formNewUser->field($model,'username') ?>                    
                <?= $formNewUser->field($model,'password')->passwordInput() ?>
                <a onClick="ShowPassword()" class="password-control">Показать пароль</a>
                <div class="form-group">
                    <?= Html::submitButton('Добавить пользователя', ['class' => 'btn btn-primary mb-10 mt-10']) ?>
                </div>
                <?php ActiveForm::end(); ?>      
            </div>      
        </div>
    </div>
</div>   
<div class="modal" id="updateModal" tabindex="-2" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Создание пользователя</h2> 
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            </div>
            <div class="modal-body">
                <?php $formNewUser = ActiveForm::begin(); ?>
                <?= $formNewUser->field($model,'email') ?>
                <?= $formNewUser->field($model,'username') ?>                    
                <?= $formNewUser->field($model,'password')->passwordInput() ?>
                <a onClick="ShowPassword()" class="password-control">Показать пароль</a>
                <div class="form-group">
                    <?= Html::submitButton('Добавить пользователя', ['class' => 'btn btn-primary mb-10 mt-10']) ?>
                </div>
                <?php ActiveForm::end(); ?>      
            </div>      
        </div>
    </div>
</div>   
<div class="site-index">

<?php $formUsers = ActiveForm::begin(); ?>
    <span id="user_span">Пользователи</span>
    <table class="table table-bordered">
        <thead>
            <trow>
                <td>Id</td>
                <td>Почта</td>
                <td>Никнейм</td>
                <td>Пароль</td>
                <td>Изменить пользователя</td>
            </trow>
        </thead>
        <?php 
            $dataProvider = new ActiveDataProvider([
            'query' => $model->find(),
            'pagination' => [
            'pageSize' => 5,
            ],
            ]);

            echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_tableUsers',
            ]);   
        ?>

    </table>
    <button type="button" class="add_user_button btn btn-primary greenLight mb-10 " data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Добавить пользователя</button>
<?php ActiveForm::end(); ?>

</div>

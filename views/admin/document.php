<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Document;
use app\widgets\CustomDropDown;

/* @var $this yii\web\View */
/* @var $users \common\models\User */
/* @var $documents \common\models\Document */
/* @var $model \common\models\User2Document */

$this->title = 'Документы';
?>
<div class="modal modal_user_documents" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Назначить документ пользователю</h2> 
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            </div>
            <div class="modal-body">
                <?php $formDocument2User = ActiveForm::begin(); ?>
                <?php echo CustomDropDown::widget(['filesDropDown'=>Document::find()->asArray()->all(),'usersDropDown'=>User::find()->asArray()->all()]); ?>                              
                <?= Html::submitButton('Добавить запись', ['class' => 'btn btn-primary']) ?> 
                <?php ActiveForm::end(); ?> 
            </div>      
        </div>
    </div>
</div> 

<?php
    $formDocs = ActiveForm::begin();
?>
    <span id="document_span">Документы</span>
    <div class="alert alert-success mt-10" role="alert">
        <span>Напоминаем, все документы лежат в '../web/documents'</span>
    </div>
    <table class="table table-bordered">
        <thead>
            <trow>
                <td>*</td>
                <td>Название документа</td>
            </trow>
        </thead>
        <?php
            $dataProvider = new ActiveDataProvider([
            'query' => $model,
            'pagination' => [
            'pageSize' => 5,
            ],
            ]);

            echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_tableDocuments',
            ]);   
        ?>
    </table>
    <button type="button" class="add_user_button btn btn-primary greenLight mb-10 " data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Назначить документ пользователю</button>
<?php ActiveForm::end(); ?>
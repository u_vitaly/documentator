<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \common\models\Test2User */
?>
<tr id="test_<?= $model->idTest?>">
    <td><?=$model->id?></td>
    <td><?=$model->getDocuments()->one()->name ?></td>
    <td><?=$model->getUsers()->one()->username ?></td>
    <td><?=$model->percentOfSuccess ?></td>
    <td><?=$model->timing ?></td>
    <td><?=Html::a('Сбросить',Url::to('/test/clear/'.$model->id)) ?></td>
</tr>

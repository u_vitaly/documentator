<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use app\models\User;

/* @var $this yii\web\View */

$this->title = 'Результаты тестирования';
?>
<?php $formTest = ActiveForm::begin(); ?>
    <span id="document_span">Результаты тестирования</span>
    <table class="table table-bordered">
        <thead>
            <trow>
                <td>*</td>
                <td>Название документа</td>
                <td>Пользователь</td>
                <td>Результат теста</td>
                <td>Время</td>
                <td>Сбросить</td>
            </trow>
        </thead>
        <?php 
            $dataProvider = new ActiveDataProvider([
            'query' => $model->find(),
            'pagination' => [
            'pageSize' => 5,
            ],
            ]);

            echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_tableTest',
            ]);   
        ?>
    </table>
<?php ActiveForm::end(); ?>
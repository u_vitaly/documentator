<?php

use yii\widgets\ActiveForm as AF;
use yii\helpers\Html;
?>

<?php
$this->title='Test';
//debug($model);
?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
<?php echo  Yii::$app->session->getFlash('success');?>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('error')): ?>
<?php echo  Yii::$app->session->getFlash('error');?>
<?php endif; ?>

<?php   $form = AF::begin(['options'=>['id'=>'form-horizontal']]) ?>
<?= $form->field($model,'name')->label('Имя')?>
<?= $form->field($model,'email')->input('email')?>
<?= $form->field($model,'text')->label('Текст сообщения')->textarea(['rows'=>'10'])?>
<?= Html::submitButton('Отправить',['class'=>'btn btn-success']) ?>
<?php   AF::end() ?>
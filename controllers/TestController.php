<?php

namespace app\controllers;

use Yii;
use yii\web\controller;
use app\models\Test;
use app\models\Document;

class TestController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) {
            return $this->redirect('login');
        }
        $userId = Yii::$app->user->identity->getId();
        $test = new Test();
        $documents = new Document();
        return $this->render('index',compact('test','documents','userId'));
        
    }

    // public function actionSelect()
    // {
    //     if(Yii::$app->request->isAjax){
    //         echo "Запроc принят!";
    //         $test2user = 
    //         Yii::$app->request->post()
    //     }
        
    // }


}

<?php

namespace app\controllers;

use Yii;
use yii\web\controller;

class MailController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $from = Yii::$app->params['adminEmail'];
        return \Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo('vitalubiyko@gmail.com')
            ->setSubject('Sus')
            ->send() 
            ? \Yii::$app->session->setFlash('success','Письмо отправлено!')
            : \Yii::$app->session->setFlash('error','Письмо не отправлено!');
    }


}

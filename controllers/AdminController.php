<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\controller;
use app\models\User;
use app\models\Document;
use app\models\User2Document;
use app\models\Test2User;
use app\models\Requester;

class AdminController extends Controller
{

    //FIXME: Fix overcoding with admin-checking

    public $layout ='admin';

    public function actionIndex()
    {
        $user = Yii::$app->user;   
        if($user->identity->isAdmin)
        {
            $model = new User();
            if(Yii::$app->request->post() && $model->save())
            {
                return $this->refresh();
            }
            return $this->render('index',compact('model'));
        }
        else
        {
            return $this->goHome();
        }
    }

    public function actionApi()
    {
        $user = Yii::$app->user;   
        if($user->identity->isAdmin)
        {
            $requesters = Requester::findAll(['isAccepted' => 0]);
            return $this->render('api',compact('requesters'));
        }
        else
        {
            return $this->goHome();
        }
    }

    public function actionDocument()
    {
        $user = Yii::$app->user;   
        if($user->identity->isAdmin)
        {
            $files = scandir('../web/documents/rtfFiles'); //FIXME: paste document path to params
            foreach($files as $file)
            {
                $info=pathinfo($file);
                $model = Document::find()->where(['name'=>$info['basename']])->one();
                if ($model!=null)
                {
                    continue;         
                }       
                $model = new Document();
                $model->name = $info['basename'];
                if(!$model->save())
                {
                    Yii::$app->session->setFlash('error', 'Ошибка добавления записи в БД!');
                }
            }  

            $model = Document::find();
            $user2document = new User2Document();
            if(Yii::$app->request->post())
            {
                debug($user2document);
                die;
                Yii::$app->session->setFlash('success','Успешно создано!');
                return $this->refresh();
            }          
            return $this->render('document',compact('model','user2document'));
        }
        else
        {
            return $this->goHome();
        }
    }

    public function actionTest()
    {    
        $model = new Test2User();
        return $this->render('test',compact('model'));
    }

}
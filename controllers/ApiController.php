<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\controller;
use app\models\Requester;

class ApiController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Documentation for API
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Request for API's key
     *
     * @return string
     */
    public function actionGetApiKey($mail)
    {
        $requester = new Requester();
        $requester->mail = $mail;
        $requester->generateApiKey();
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionGetDocument($file,$apiKey)
    {
        //if(Requester::findByApiKey($apiKey))
            //TODO:Загружаем файл
        //TODO:Возвращаем HTML
    }

}
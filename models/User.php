<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
* This is the model class for table "user".
*
* @property integer $id
* @property string $username
* @property string $email
* @property string $authKey
* @property string $isAdmin
* @property integer $created_at
* @property integer $updated_at
* @property int $current_document->id
*/

class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'username'=>'Логин',
            'password'=>'Пароль',
            'email'=>'Электронная почта',
            'isAdmin'=>'Администратор',
            'current_document_id' => 'ID последнего изучаемого документа'
        ];
    }

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email','username','password'], 'required'],
            [['email','username'],'string','max'=>127],
            [['email','username'],'unique'],
            [['password'],'validatePassword'],
            [['password'],'string','min'=>6,'max'=>127]
        ];
    }

    public function validatePassword($password)
    {
        return $password===User::findByUsername($this->username)->password;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

        /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUserName()
    {
        return $this->username;
    }
    
    public function getUserId()
    {
        return $this->id;
    }

    public function getCurrentDocumentId()
    {
        return $this->current_document_id;
    }

    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validateOldPassword($attribute, $params)
    {
        if (!$this || !$this->validatePassword($this->$attribute)) {

            $this->addError($attribute, 'Введен неверный пароль');
        }
    }

    public function getTest2User()
    {
        return $this->hasOne(Test2User::class, ['idUser' => 'id']);

    }

    

}

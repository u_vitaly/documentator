<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\queries\DocumentQuery;

/**
* This is the model class for table "document".
*
* @property integer $id
* @property string $name
* @property boolean $hasNeiroAdaptation

* @property User[] $user
*/

class Document extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'name'=>'Название документа',
            'hasNeiroAdaptation'=>'Обработан ли нейросетью',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function find()
    {
        return new DocumentQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUser()
    {
        return $this->hasMany(User::class, ['id' => 'idUser'])
        ->via('user2document');

    }

    public function getUser2document()
    {
        return $this->hasMany(User2Document::class, ['idDocument' => 'id']);

    }


    

}

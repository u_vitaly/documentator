<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\queries\TestQuery;
use app\models\Question;

/**
* This is the model class for table "answer".
*
* @property integer $id
* @property string  $name

* @property Question[]  $questions
*/

class Test extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'name'=>'Название'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function find()
    {
        return new TestQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getQuestions()
    {
        return $this->hasMany(Question::class, ['id' => 'idQuestion']);

    }

    public function getTest2User()
    {
        return $this->hasOne(Test2User::class, ['idTest' => 'id']);

    }

    

}

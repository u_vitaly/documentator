<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\Answer;
use app\models\Test;
use app\models\queries\QuestionQuery;

/**
* This is the model class for table "question".
*
* @property integer $id
* @property integer $idTest
* @property string  $name

* @property Test  $test
* @property Answer[]  $answers
*/

class Question extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'name'=>'Ответ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function find()
    {
        return new QuestionQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answer}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAnswers()
    {
        return $this->hasMany(Answer::class, ['idQuestion' => 'id']);

    }

    public function getTest()
    {
        return $this->hasOne(Test::class, ['id' => 'idTest']);

    }

    

}

<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\queries\AnswerQuery;
use app\models\Question;

/**
* This is the model class for table "answer".
*
* @property integer $id
* @property integer $idQuestion
* @property string  $name
* @property boolean $isTrue

* @property Question  $question
*/

class Answer extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'name'=>'Ответ',
            'isTrue'=>'Правильный ответ',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function find()
    {
        return new AnswerQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%answer}}';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::class, ['id' => 'idQuestion']);

    }

    

}

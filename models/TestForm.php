<?php

namespace app\models;
use yii\base\Model;

class TestForm extends Model {

    public $name;
    public $email;
    public $text;


    public function attributeLabels() {
        return [
            'name'=> 'Имя',
            'email'=> 'E-mail',
            'text'=> 'Текст сообщения',
        ];
    }

    public function rules()
    {

        return [
            [['name','email'],'required'],
            ['email','email'],
            //['name','string','min'=>2,'tooShort'=>'Мало'],
            //['name','string','max'=>20,'tooLong'=>'Wrong'],
            ['name','string','length'=>[2,5]],
            ['name','myRule'],
        ];
    }
    
    public function myRule($attr)
    {
    
        if(!in_array($this->$attrs,['hello','world']))
        {
            $this->addError($attrs,'Wrong!');
        }
    
    }

}   


?>
<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use app\models\queries\Test2UserQuery;

/**
* This is the model class for table "test2user".
*
* @property integer $id
* @property integer $idTest
* @property integer $idUser
* @property integer $idDocument
* @property boolean $isSuccess
* @property integer $percentOfSuccess 
* @property integer $timing 

* @property Test[] $tests
* @property User[] $users 
* @property Document[] $documents
*/

class Test2User extends ActiveRecord
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'idTest'=>'ID теста',
            'isSuccess'=>'Успешно',
            'percentOfSuccess'=>'Результат',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test2user';
    }

    public static function find()
    {
        return new Test2UserQuery(static::class);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function getIdTest()
    {
        return $this->idTest;
    }

    public function getTiming()
    {
        return $this->timing;
    }

    public function getIdDocument()
    {
        return $this->idDocument;
    }

    public function getIsSuccess()
    {
        return $this->idSuccess;
    }


    public function getTests()
    {
        return $this->hasMany(Test::class, ['id' => 'idTest']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'idUser']);
    }

    public function getDocuments()
    {
        return $this->hasMany(Document::class, ['id' => 'idDocument']);
    }

}

<?php

namespace app\models;


use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/**
* This is the model class for table "user2document".
*
* @property integer $id
* @property integer $idUser
* @property integer $idDocument
* @property string $spendedTime
* @property boolean $isLearned
* @property User $user
* @property User[] $users
* @property Document $document
* @property Document[] $documents
*/

class User2Document extends ActiveRecord
{

    const STATUS_ADAPTED = 1;
    const STATUS_FREE = 0   ;
    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'idUser'=>'ID пользователя',
            'idDocument'=>'ID документа',
            'hasNeiroAdaptation'=>'Обработан нейронной сетью',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idUser,idDocument'], 'required'],
            [['idUser,idDocument'],'integer'],
            ['isLearned','boolean'],
            ['spendedTime','string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user2document';
    }

    public function getIdUser()
    {
        return $this->idUser;
    }

    public function getSpendedTime()
    {
        return $this->spendedTime;
    }

    public function getIsLearned()
    {
        return $this->isLearned;
    }

    public function getIdDocument()
    {
        return $this->idDocument;
    }

    public function getDocuments($idUser)
    {
        return $this->hasMany(Document::class, ['idUser' => 'id'])->where(['idUser'=>$idUser]);
    }

    public function getUsers($idDocument)
    {
        return $this->hasMany(User::class, ['idDocument' => 'id'])->where(['idDocument'=>$idDocument]);
    }

    public function beforeSave($insert)
    {
        if($insert)
        {
            $this->spendedTime = 0;
            $this->isLearned = 0;
        }
        parent::beforeSave($insert);
    }


    

}

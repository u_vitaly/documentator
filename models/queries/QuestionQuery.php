<?php

namespace app\models\queries;

use common\dto\search\ProductSearchDto;
use common\models\Product;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class AnswerQuery
 * @package app\models\queries
 */
class QuestionQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|ActiveRecord[]|Product[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|ActiveRecord|null|Product
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function byIdTest($idTest): self
    {
        return $this->joinWith('question')
            ->andWhere(['test.id' => $idTest]);
    }

}

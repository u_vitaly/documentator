<?php

namespace app\models\queries;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class TestQuery
 * @package app\models\queries
 */
class TestQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|ActiveRecord[]|Product[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|ActiveRecord|null|Product
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function byName($name): self
    {
        return $this->andWhere(['name' => $name]);
    }

}

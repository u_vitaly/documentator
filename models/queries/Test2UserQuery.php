<?php

namespace app\models\queries;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Test2UserQuery
 * @package app\models\queries
 */
class Test2UserQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|ActiveRecord[]|Product[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|ActiveRecord|null|Product
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function byIdtest($idTest): self
    {
        return $this->andWhere(['idTest' => $idTest]);
    }

    /**
     * @return $this
     */
    public function byIdUser($idUser): self
    {
        return $this->andWhere(['idUser' => $idUser]);
    }

    /**
     * @return $this
     */
    public function byIdDocument($idDocument): self
    {
        return $this->andWhere(['idDocument' => $idDocument]);
    }


}

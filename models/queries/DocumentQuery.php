<?php

namespace app\models\queries;

use common\dto\search\ProductSearchDto;
use common\models\Product;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class DocumentQuery
 * @package app\models\queries
 */
class DocumentQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|ActiveRecord[]|Product[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param null $db
     * @return array|ActiveRecord|null|Product
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function byUserId($userId): self
    {
        return $this->joinWith('user')
            ->viaTable('user2document',['id'=>'idUser'])
            ->andWhere(['idUser' => $userId]);
    }

}

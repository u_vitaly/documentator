<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "Requester".
*
* @property integer $id
* @property string $email
* @property string $isAccepted
* @property string $apiKey
*/

class Requester extends ActiveRecord 
{

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'email'=>'Электронная почта',
            'isAccepted'=>'Статус подтверждения',
        ];
    }

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'unique'],
            [['isAccepted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds requester by apiKey
     *
     * @param string $email
     * @return static|null
     */
    public static function findByApiKey($apiKey)
    {
        return static::findOne(['apiKey' => $apiKey]);
    }

    public function generateApiKey()
    {
        $this->apiKey = ''; //TODO: Генерация ключа
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function beforeSave($insert)
    {
        $this->isAccepted=0;
        parent::beforeSave($insert);
    }

    public function getApiKey()
    {
        return $this->apiKey;
    } 

}

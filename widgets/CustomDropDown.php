<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class CustomDropDown extends Widget
{
    /**
     * @var array Все выпадаемыве списки => ['id'=>'value']
     */
    public $filesDropDown = [];

      /**
     * @var array Все выпадаемыве списки => ['id'=>'value']
     */
    public $usersDropDown = [];


    /**
     * {@inheritdoc}
     */
    public function run()
    {   
        $files='';
        foreach($this->filesDropDown as $id=>$value)
        {
            $files=$files.'<li><input type="checkbox" id="'.$value["id"].'" value="'.$value["id"].'"  name="User2Document[idDocument]"><a class="dropdown-item" href="#">'.$value["name"].'</a></li>';
        }
        $users='';
        foreach($this->usersDropDown as $id=>$value)
        {
            $users=$users.'<li><input type="radio" id="'.$value["id"].'" value="'.$value["id"].'"  name="User2Document[idUser]"><a class="dropdown-item" href="#">'.$value["username"].'</a></li>';
        }
        
        echo    '<div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                        Выберите файлы
                    </button>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">'.
                    $files.'
                    </ul>
                </div>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                        Выберите пользователя
                    </button>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">'.
                    $users.'
                    </ul>
                </div>';
       
    }
}

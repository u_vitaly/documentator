<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class TestDropdown extends Widget {

    public $documents;
    public $caret;

    public function init() {
        parent::init();
        if ($this->caret === null) {
            $this->caret = true;
        }
    }

    public function run() {
        return $this->render('_dropdown', [
            'caret' => $this->caret,
            'documents' => $this->documents
        ]);
    }
}

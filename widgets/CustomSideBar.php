<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class CustomSideBar extends Widget
{
    /**
     * @var array Выводимый текст в меню => [Controller/action,...]
     */
    public $titlesActions = [];

      /**
     * @var array Выводимый текст в меню => путь до иконки
     */
    public $titlesIcons = [];


    /**
     * {@inheritdoc}
     */
    public function run()
    {        
        $content='';
        foreach($this->titlesActions as $title=>$action)
        {
            $content=$content.'
            <li class="nav-item">
                <a class="nav-link" href="'.$action.'">
                    <img src="'.$this->titlesIcons[$title].'" alt="">
                    <span>'.$title.'</span>
                </a>
            </li>
            ';
        }     
        $navbar =   '<nav class="col-md-2 d-none d-md-block bg-light sidebar">
                        <div class="sidebar-sticky">
                            '. $content.'
                        </div>
                    </nav>';
        return $navbar;
       
    }
}

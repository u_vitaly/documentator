<?php
/**
 * @var Document[] $documents
 */

$caret = $caret ? '<span class="caret"></span>' : '';
?>
<?php $count = count($documents)?> 
<?php $default='Выбрать документ';?>
<?php $notOne = $count >= 1 ?>
<div id="dropdown_documents" class="dropdown mt-10">
        <span class="documents_dropdown" role="button" <?= $notOne ? 'aria-haspopup="true" data-toggle="dropdown"' : 'aria-haspopup="false"' ?> aria-expanded="false">
            <?= $notOne ? $default : '' ?><?= $notOne ? $caret : '' ?>
        </span>
        <ul class="dropdown-menu documents_list">
            <?php
            foreach ($documents as $document)
                echo '<li data-id='.$document["id"].' style="padding: 5px" class="test_clickable">'. $document['name'] .'</li>'; //TODO:Пофиксил, там не хватало кавычки в конце
            ?>
        </ul>
</div>

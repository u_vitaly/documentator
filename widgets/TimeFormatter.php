<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class TimeFormatter extends Widget
{
    const IN_DAY = 24;
    const IN_HOUR = 60;
    const IN_MINUTE = 60;
    
    public $seconds;
    /**
     * {@inheritdoc}
     */
    public function run()
    {  
        $time = $this->getStandartTime($this->seconds);
        $days = $time['days'] ? $time['days'].' дней,' : '';
        $hours = $time['hours'] ? $time['hours'].' часов, ' : '';
        $minutes = $time['minutes'] ? $time['minutes'].' минут, ' : '';
        $seconds = $time['seconds'] ? $time['seconds'].' секунд ' : 'Неизвестно';
        echo '<p>Затраченное время:</p>
        <p>'
            .$days.$hours.$minutes.$seconds;
        '</p>';
    }

    private function getStandartTime($seconds)
    {
        if(!$seconds)
            return 'There is no time to format!';
        
        $days = $seconds / self::IN_DAY / self::IN_HOUR / self::IN_MINUTE;
        $lostSeconds = $seconds % (self::IN_DAY * self::IN_HOUR * self::IN_MINUTE);

        $hours = $lostSeconds / self::IN_HOUR / self::IN_MINUTE;
        $lostSeconds = $lostSeconds % (self::IN_HOUR * self::IN_MINUTE);

        $minutes = $lostSeconds / self::IN_MINUTE;
        $seconds = $lostSeconds % self::IN_MINUTE;

        return ['days'=>intval($days),'hours'=>intval($hours),'minutes'=>intval($minutes),'seconds'=>$seconds];
    }
}
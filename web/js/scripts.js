    function PasswordGenerate()
    {
        var randomstring = Math.random().toString(36).slice(-8);
        $('#user-password').val(randomstring);
    }

    //Показать/скрыть пароль
    function ShowPassword()
    {
        if ($('#user-password').attr('type') == 'password'){
            $(this).html('Скрыть пароль');
            $('#user-password').attr('type', 'text');
        } else {
            $(this).html('Показать пароль');
            $('#user-password').attr('type', 'password');
        }
        return false;
    }

    $('.add_user_button').click(function(){
        PasswordGenerate();
    });

    $('.test_clickable').click(function(){
        $('.documents_dropdown').text((i, val) => $(this).text());
        var id = $(this).data('id');
        $('.btn_test').addClass('btn_test__true');
        $('.btn_test').removeClass('btn_test__wrong');
        var link =  $('.btn_test').children().first();
        var stockLink = '/test/'
        link.attr('href',stockLink+id);
    });

